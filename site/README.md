# Microscope Friday
This is a placeholder for Microscope Friday's docs.

It will include cards like this, really:

```markdown
::: event-dark
A plague wipes out 90% of the nation
:::
```

::: event-dark
A plague wipes out 90% of the nation
:::

and this

```markdown
::: event-light
A new energy source radically alters the way the people live
:::
```

::: event-light
A new energy source radically alters the way the people live
:::

## Let's try something more in depth

<period summary="Nuclear war ravages the land" tone="dark">
    <event summary="Nuclear war is declared." tone="dark">
        <scene question="What happened when..." scene="General's chambers" answer="Aliens told me!"></scene>
    </event>
</period>