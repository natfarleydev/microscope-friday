function makeSvgCircle(size, fillColor, classes) {
    return `<svg height="${size}" width="${size}" class="${classes}">
        <circle cx="${size/2}" cy="${size/2}" r="${size*0.8/2}" stroke="black" stroke-width="3" fill="${fillColor}" />
        </svg>`
}

function makeCard(type, color, circleSize) {
  circleSize = circleSize || 100

  return function(tokens, idx) {
    if (tokens[idx].nesting === 1) {
      // opening tag
      return (`<div class="${type}-container"><div class="${type}">`)
    } else {
      // closing tag
      return (
          makeSvgCircle(circleSize, color, `${type}-circle`)
          + '</div></div>\n'
      );
    }
  }
}

module.exports = {
  title: 'Microscope Friday',
  description: 'Information on the Microscope Friday campaign setting.',
  base: '/microscope-friday/',
  dest: 'public',
   themeConfig: {
     sidebar: ['/', 'thegame.html'],
     nav: [{ text: 'Home', link: '/' }, { text: 'The game', link: './thegame.html' }]
   },
  markdown: {
    config: md => {
      md.use(require('markdown-it-container'), 'period-dark', {
        render: makeCard('period', 'black')
      })
      md.use(require('markdown-it-container'), 'period-light', {
        render: makeCard('period', 'white')
      })
      md.use(require('markdown-it-container'), 'event-dark', {
        render: makeCard('event', 'black')
      })
      md.use(require('markdown-it-container'), 'event-light', {
        render: makeCard('event', 'white')
      })
      md.use(require('markdown-it-container'), 'scene-dark', {
        render: makeCard('scene', 'black', 20)
      })
      md.use(require('markdown-it-container'), 'scene-light', {
        render: makeCard('scene', 'white', 20)
      })
    }
  }
};
